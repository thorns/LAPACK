#! /bin/bash

################################################################################
# Prepare
################################################################################

# Set up shell
if [ "$(echo ${VERBOSE} | tr '[:upper:]' '[:lower:]')" = 'yes' ]; then
    set -x                      # Output commands
fi
set -e                          # Abort on errors



################################################################################
# Configure Cactus
################################################################################

pkg-config --exists lapack || exit 1
LAPACK_INC_DIRS=$(pkg-config --cflags-only-I lapack | sed 's/-I//g')
LAPACK_LIB_DIRS=$(pkg-config --libs-only-L lapack | sed 's/-L//g')
LAPACK_LIBS=$(pkg-config --libs-only-l lapack | sed 's/-l//g')

# Pass options to Cactus
echo "BEGIN MAKE_DEFINITION"
echo "HAVE_LAPACK     = 1"
echo "LAPACK_DIR      = ${LAPACK_DIR}"
echo "LAPACK_INC_DIRS = ${LAPACK_INC_DIRS}"
echo "LAPACK_LIB_DIRS = ${LAPACK_LIB_DIRS}"
echo "LAPACK_LIBS     = ${LAPACK_LIBS}"
echo "END MAKE_DEFINITION"

echo 'INCLUDE_DIRECTORY $(LAPACK_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(LAPACK_LIB_DIRS)'
echo 'LIBRARY           $(LAPACK_LIBS)'
